# mysqlPart

mysqlPart adalah bagian dari aplikasi peminjaman buku myLibApp yang khusus memproses tabel borrows dan customers. Customers memiliki fungsionalitas CRUD (Create, Read, Update, Delete) serupa dengan tabel books di mongopart. Sedangkan Borrows memiliki fungsionalitas mencatat peminjaman baru, user yang meminjam, buku yang dipinjam, dan status peminjaman (masih aktif/sedang meminjam atau sudah tidak aktif/sudah dikembalikan). Adapun API yang digunakan adalah Flask dengan keamanan JWT tanpa ORM
